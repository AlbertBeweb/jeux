<?php ob_start(); ?>
<?php  require_once("../includes/db.php"); ?>
<?php
// Connexion à la base de données
try
{
	$bdd = new PDO('mysql:host=localhost;dbname=tchat;charset=utf8', 'nicolas', '');
}
catch(Exception $e)
{
        die('Erreur : '.$e->getMessage());
}

// Insertion du message à l'aide d'une requête préparée
$req = $bdd->prepare('INSERT INTO minichat (pseudo, message) VALUES(?, ?)');
$req->execute(array($_POST['pseudo'], $_POST['messages']));

// Redirection du visiteur vers la page du minichat
header('Location: minichat.php');
?>