<?php ob_start(); ?>
<?php  require_once("./includes/db.php"); ?>
<?php require_once("./includes/header.php"); ?>

<body>

  <?php require_once("./includes/navigation.php"); ?>


    <!-- Page Content -->
    <div class="container">

      <!-- Jumbotron Header -->
      <header class="jumbotron my-4">
        <h1 class="display-3 text-center">Bienvenue sur Jeux Vidéos</h1><br/>
        
        <?php require_once("./includes/widget.php"); ?>
        
        <p class="lead"></p>

      </header>

      <!-- Page Features -->
      <div class="row text-center">
      <?php
          try
          {
              
          }
          catch(Exception $e)
          {
          // En cas d'erreur, on affiche un message et on arrête tout
                  die('Erreur : '.$e->getMessage());
          }
          // Si tout va bien, on peut continuer

          // On récupère tout le contenu de la table jeux_video
          $reponse = $bdd->query('SELECT * FROM jeux_video ORDER BY ID DESC LIMIT 0, 90');
          // On affiche chaque entrée une à une
          while ($donnees = $reponse->fetch())
          {
      ?>
          <!-- CARD -->
          <div class="col-lg-3 col-md-6 mb-4">
            <div class="card">
              <img class="card-img-top img-responsive" src="<?php echo $donnees['images']; ?>" alt="">
              <div class="card-body">
                <h4 class="card-title"><?php echo $donnees['nom']; ?></h4>
                <p></p>
                <p class="card-text"><?php echo $donnees['prix']; ?> euros !</p>
              </div>
              <div class="card-footer">
                <a href="#" class="btn btn-primary">Voir plus</a>
              </div>
            </div>
          </div>
        <?php
            }
            $reponse->closeCursor(); // Termine le traitement de la requête
        ?>
        
      
        </div>
      <!-- /.row -->
    </div>
  <!-- /.container -->

<?php require_once('./includes/footer.php') ?>
