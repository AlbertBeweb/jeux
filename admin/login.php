<?php ob_start(); ?>
<?php
  setcookie('users_email', 'admin', time() + 365*24*3600, null, null, false, true); // On écrit un cookie
  setcookie('users_password', '123', time() + 365*24*3600, null, null, false, true); // On écrit un autre cookie...
?>
<?php  require_once("../includes/db.php"); ?>
<?php require_once("./includes/header_admin.php"); ?>



<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Connection</div>
      <div class="card-body">
        <form method="post" action="index.php">        
        <div class="form-group">
            <label for="exampleInputEmail1">E-Mail</label>
            <input name="users_email" class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="<?php echo $_COOKIE['users_email']; ?>">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Mot de passe</label>
            <input name="users_password" class="form-control" id="exampleInputPassword1" type="password" placeholder="<?php echo $_COOKIE['users_password']; ?>">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Se souvenir de moi</label>
            </div>
          </div>
          <a class="btn btn-primary btn-block" type="submit" href="index.php">Envoyer</a>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register.html">Créer un compte</a>
          <a class="d-block small" href="forgot-password.html">Mot de passe oublié</a>
          <a class="d-block small" href="../index.php">Retour vers le site</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
