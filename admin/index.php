<?php ob_start(); ?>
<?php  require_once("../includes/db.php"); ?>
<?php  require_once("includes/header_admin.php"); ?>



<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  
  <!-- Navigation-->
  <?php  require_once("includes/navigation_admin.php"); ?>

  
  <div class="content-wrapper">
    <div class="container-fluid">
      
     
     
      <div class="row">
        <div class="col-lg-12">
         
           
            
     <!-- DATA TABLE -->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Liste de jeux</div>
        <div class="card-body">
          <div class="table-responsive">
        
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                <th class="text-center">#Id</th>
                  <th class="text-center">Nom</th>
                  <th class="text-center">Prix</th>
                  <th class="text-center">Commentaires</th>
                  <th class="text-center">Supprimer</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th class="text-center">#Id</th>
                  <th class="text-center">Nom</th>
                  <th class="text-center">Prix</th>
                  <th class="text-center">Commentaires</th>
                  <th class="text-center">Supprimer</th>
                </tr>
              </tfoot>
              <?php
                    try
                    {
                        
                    }
                    catch(Exception $e)
                    {
                    // En cas d'erreur, on affiche un message et on arrête tout
                            die('Erreur : '.$e->getMessage());
                    }
                    // Si tout va bien, on peut continuer

                    // On récupère tout le contenu de la table jeux_video
                    $reponse = $bdd->query('SELECT * FROM jeux_video LIMIT 0, 90');
                    // On affiche chaque entrée une à une
                    while ($donnees = $reponse->fetch())
                    {
                ?>
              <tbody>
                <tr>
                  <td class="text-center"><?php echo $donnees['ID']; ?></td>
                  <td class="text-center"><?php echo $donnees['nom']; ?></td>
                  <td class="text-center"><?php echo $donnees['prix']; ?></td>
                  <td class="text-center"><?php echo $donnees['commentaires']; ?></td>
                  <td class="text-center"><button type="button" class="btn btn-lg btn-danger">Supprimer</button></td>
                </tr>
              </tbody>
              <?php
            }
            $reponse->closeCursor(); // Termine le traitement de la requête
        ?>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    
    <!-- ADMIN_FOOTER -->
<?php  require_once("includes/footer_admin.php"); ?>